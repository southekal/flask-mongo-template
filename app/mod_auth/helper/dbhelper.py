from flask_login import LoginManager, login_user, logout_user, current_user, login_required

from app.mod_auth.models import User, PasswordHashing


def user_exists(email):
    u = User.objects(email=email)
    if len(u) > 0:
        return True
    else:
        return False


def get_user_details(email):
    fields = ["id", "email", "name", "created"]
    data_holder = {}
    _user_exists = user_exists(email)
    if _user_exists:
        u = User.objects(email=current_user.get_id())[0]
        for f in fields:
            data_holder[f] = u[f]
    return data_holder


def get_user_password(email):
    _user_exists = user_exists(email)
    if _user_exists:
        u = User.objects(email=email)[0]
        return u["password"]
    else:
        return None


def password_match(pw, email):
    _pw = get_user_password(email)
    if _pw:
        p = PasswordHashing(password=pw).check_password(_pw)
        return p
    else:
        return False



