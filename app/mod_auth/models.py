import datetime

from app import db
from mongoengine import *
from werkzeug import check_password_hash, generate_password_hash


class User(db.Document):
    email = db.StringField(required=True, unique=True)
    name = db.StringField(max_length=50)
    password = db.StringField(max_length=300)
    created = db.DateTimeField(default=datetime.datetime.now)

    def is_active(self):
        """True, as all users are active."""
        return True

    def get_id(self):
        """Return the email address to satisfy Flask-Login's requirements."""
        return self.email

    def is_authenticated(self):
        """Return True if the user is authenticated."""
        return self.authenticated

    def is_anonymous(self):
        """False, as anonymous users aren't supported."""
        return False


class Post(db.Document):
    title = db.StringField(required=True)
    content = db.StringField(required=True)
    author = db.ReferenceField(User, reverse_delete_rule=CASCADE)


class PasswordHashing(object):
    def __init__(self, password):
        self.password = password

    def set_password(self):
        pw_hash = generate_password_hash(self.password)
        return pw_hash

    def check_password(self, pw_hash):
        return check_password_hash(pw_hash, self.password)


