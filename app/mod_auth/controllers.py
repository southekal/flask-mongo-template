from flask import Blueprint, Markup, request, render_template, flash, g, session, redirect, url_for
from flask_login import LoginManager, login_user, logout_user, current_user, login_required
from werkzeug.exceptions import HTTPException, NotFound
import validators

from app.mod_auth.forms import SignUp, Login

from app.mod_auth.models import User, PasswordHashing
from app.mod_auth.helper import dbhelper

mod_auth = Blueprint('auth', __name__, url_prefix='')

from app import login_manager
from app import logger


@login_manager.user_loader
def load_user(email):
    try:
        return User.objects.get_or_404(email=email)
    except NotFound as e:
        logger.error('Could not load user - "{}"'.format(e))
        return "404.html", 404


@mod_auth.route('/signup', methods=['GET', 'POST'])
def signup():
    if current_user.is_authenticated:
        return redirect('/')
    form = SignUp(request.form)
    if request.method == "POST" and form.validate_on_submit():
        # checks of email structure is valid
        is_valid_email = validators.email(form.email.data)
        if not dbhelper.user_exists(email=form.email.data) and is_valid_email:
            _hash = PasswordHashing(form.password.data).set_password()
            u = User(email=form.email.data, name=form.name.data, password=_hash).save()
            # user authentication
            login_user(u)
            return redirect('/')
        elif not is_valid_email:
            flash("email is invalid", "danger")
        else:
            flash("Email already exists! <b><a href='/login'>Login</a></b> to access it", "danger")
    return render_template("auth/signup.html", form=form)


@mod_auth.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect("/")

    form = Login(request.form)
    if request.method == "POST" and form.validate_on_submit():
        _user_exists = dbhelper.user_exists(email=form.email.data)
        if _user_exists:
            _check_hash = dbhelper.password_match(form.password.data, form.email.data)
            if _check_hash:
                u = User.objects(email=form.email.data)
                # user authentication
                login_user(u[0])
                flash('Logged in successfully!', 'success')
                return redirect('/')
            else:
                flash('Credentials do not match!', 'danger')
        else:
            flash('User does not exist! Create an account <b><a href="/signup">here</a></b>', "danger")
    return render_template("auth/login.html", form=form)


@mod_auth.route('/profile', methods=['GET', 'POST'])
@login_required
def profile():
    user = User.objects(email=current_user.get_id())[0]
    return render_template("auth/profile.html", user=user)


@mod_auth.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect("/")